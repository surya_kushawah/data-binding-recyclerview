package com.example.databinding;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import com.example.databinding.model.Employee;
import com.example.databinding.model.EmployeeRepository;
import java.util.List;

/**
 * Created by Morris on 03,June,2019
 */
public class MainActivityViewModel extends AndroidViewModel {
  private EmployeeRepository employeeRepository;

  public MainActivityViewModel(@NonNull Application application) {
    super(application);
    employeeRepository = new EmployeeRepository(application);
  }

  public LiveData<List<Employee>> getAllEmployee() {
    return employeeRepository.getMutableLiveData();
  }
}
