package com.example.databinding.model;

import android.databinding.BindingAdapter;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.databinding.R;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Morris on 05,June,2019
 */
public class Employee implements Parcelable {

  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("email")
  @Expose
  private String email;
  @SerializedName("first_name")
  @Expose
  private String firstName;
  @SerializedName("last_name")
  @Expose
  private String lastName;
  @SerializedName("avatar")
  @Expose
  private String avatar;

  @BindingAdapter({ "avatar" })
  public static void loadImage(ImageView imageView, String imageURL) {

    Glide.with(imageView.getContext())
        .setDefaultRequestOptions(new RequestOptions()
            .circleCrop())
        .load(imageURL)
        .placeholder(R.drawable.loading)
        .into(imageView);
  }

  public final static Parcelable.Creator<Employee> CREATOR = new Creator<Employee>() {

    @SuppressWarnings({
        "unchecked"
    })
    public Employee createFromParcel(Parcel in) {
      return new Employee(in);
    }

    public Employee[] newArray(int size) {
      return (new Employee[size]);
    }
  };

  protected Employee(Parcel in) {
    this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
    this.email = ((String) in.readValue((String.class.getClassLoader())));
    this.firstName = ((String) in.readValue((String.class.getClassLoader())));
    this.lastName = ((String) in.readValue((String.class.getClassLoader())));
    this.avatar = ((String) in.readValue((String.class.getClassLoader())));
  }

  public Employee() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(id);
    dest.writeValue(email);
    dest.writeValue(firstName);
    dest.writeValue(lastName);
    dest.writeValue(avatar);
  }

  public int describeContents() {
    return 0;
  }
}
