package com.example.databinding.model;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;
import com.example.databinding.network.EmployeeDataService;
import com.example.databinding.network.RetrofitClient;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Morris on 05,June,2019
 */
public class EmployeeRepository {
  private static final String TAG = "EmployeeRepository";
  private ArrayList<Employee> employees = new ArrayList<>();
  private MutableLiveData<List<Employee>> mutableLiveData = new MutableLiveData<>();
  private Application application;

  public EmployeeRepository(Application application) {
    this.application = application;
  }

  public MutableLiveData<List<Employee>> getMutableLiveData() {

    final EmployeeDataService userDataService = RetrofitClient.getService();

    Call<EmployeeDBResponse> call = userDataService.getEmployees();
    call.enqueue(new Callback<EmployeeDBResponse>() {
      @Override
      public void onResponse(Call<EmployeeDBResponse> call, Response<EmployeeDBResponse> response) {
        Log.e(TAG, "onResponse: " + response.body());
        EmployeeDBResponse employeeDBResponse = response.body();
        if (employeeDBResponse != null && employeeDBResponse.getEmployees() != null) {
          employees = (ArrayList<Employee>) employeeDBResponse.getEmployees();
          mutableLiveData.setValue(employees);
          Log.e(TAG, "onResponse: " + response.body());
        }
      }

      @Override
      public void onFailure(Call<EmployeeDBResponse> call, Throwable t) {
        Log.e(TAG, "onFailure: " );
      }
    });

    return mutableLiveData;
  }
}