package com.example.databinding.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by Morris on 05,June,2019
 */
public class EmployeeDBResponse implements Parcelable {

  @SerializedName("page")
  @Expose
  private Integer page;
  @SerializedName("per_page")
  @Expose
  private Integer perPage;
  @SerializedName("total")
  @Expose
  private Integer total;
  @SerializedName("total_pages")
  @Expose
  private Integer totalPages;
  @SerializedName("data")
  @Expose
  private List<Employee> employee = null;
  public final static Parcelable.Creator<EmployeeDBResponse> CREATOR =
      new Creator<EmployeeDBResponse>() {

        @SuppressWarnings({
            "unchecked"
        })
        public EmployeeDBResponse createFromParcel(Parcel in) {
          return new EmployeeDBResponse(in);
        }

        public EmployeeDBResponse[] newArray(int size) {
          return (new EmployeeDBResponse[size]);
        }
      };

  protected EmployeeDBResponse(Parcel in) {
    this.page = ((Integer) in.readValue((Integer.class.getClassLoader())));
    this.perPage = ((Integer) in.readValue((Integer.class.getClassLoader())));
    this.total = ((Integer) in.readValue((Integer.class.getClassLoader())));
    this.totalPages = ((Integer) in.readValue((Integer.class.getClassLoader())));
    in.readList(this.employee, (com.example.databinding.model.Employee.class.getClassLoader()));
  }

  public EmployeeDBResponse() {
  }

  public Integer getPage() {
    return page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getPerPage() {
    return perPage;
  }

  public void setPerPage(Integer perPage) {
    this.perPage = perPage;
  }

  public Integer getTotal() {
    return total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }

  public Integer getTotalPages() {
    return totalPages;
  }

  public void setTotalPages(Integer totalPages) {
    this.totalPages = totalPages;
  }

  public List<Employee> getEmployees() {
    return employee;
  }

  public void setEmployees(List<Employee> data) {
    this.employee = data;
  }

  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(page);
    dest.writeValue(perPage);
    dest.writeValue(total);
    dest.writeValue(totalPages);
    dest.writeList(employee);
  }

  public int describeContents() {
    return 0;
  }
}

