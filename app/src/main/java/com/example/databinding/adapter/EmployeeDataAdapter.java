package com.example.databinding.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.example.databinding.R;
import com.example.databinding.databinding.EmployeeListItemBinding;
import com.example.databinding.model.Employee;
import java.util.ArrayList;

public class EmployeeDataAdapter
    extends RecyclerView.Adapter<EmployeeDataAdapter.EmployeeViewHolder> {

  private ArrayList<Employee> employees;

  @NonNull
  @Override
  public EmployeeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    EmployeeListItemBinding employeeListItemBinding =
        DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
            R.layout.employee_list_item, viewGroup, false);
    return new EmployeeViewHolder(employeeListItemBinding);
  }

  @Override
  public void onBindViewHolder(@NonNull EmployeeViewHolder employeeViewHolder, int i) {
    Employee currentStudent = employees.get(i);
    employeeViewHolder.employeeListItemBinding.setEmployee(currentStudent);
  }

  @Override
  public int getItemCount() {
    if (employees != null) {
      return employees.size();
    } else {
      return 0;
    }
  }

  public void setEmployees(ArrayList<Employee> employees) {
    this.employees = employees;
    notifyDataSetChanged();
  }

  class EmployeeViewHolder extends RecyclerView.ViewHolder {

    private EmployeeListItemBinding employeeListItemBinding;

    public EmployeeViewHolder(@NonNull EmployeeListItemBinding employeetListItemBinding) {
      super(employeetListItemBinding.getRoot());

      this.employeeListItemBinding = employeetListItemBinding;
    }
  }
}
