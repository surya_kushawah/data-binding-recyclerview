package com.example.databinding.network;

import com.example.databinding.model.EmployeeDBResponse;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Morris on 05,June,2019
 */
public interface EmployeeDataService {
  @GET("users/?per_page=12&page=1")
  Call<EmployeeDBResponse> getEmployees();
}
