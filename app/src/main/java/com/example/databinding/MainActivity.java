package com.example.databinding;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import com.example.databinding.adapter.EmployeeDataAdapter;
import com.example.databinding.databinding.ActivityMainBinding;
import com.example.databinding.model.Employee;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  private MainActivityViewModel mainActivityViewModel;
  private EmployeeDataAdapter employeeDataAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ActivityMainBinding activityMainBinding =
        DataBindingUtil.setContentView(this, R.layout.activity_main);

    // bind RecyclerView
    RecyclerView recyclerView = activityMainBinding.viewEmployees;
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    recyclerView.setHasFixedSize(true);

    mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
    employeeDataAdapter = new EmployeeDataAdapter();
    recyclerView.setAdapter(employeeDataAdapter);

    getAllEmployee();
  }

  private void getAllEmployee() {
    Log.e("getAllEmployee", "getAllEmployee: ");
    mainActivityViewModel.getAllEmployee().observe(this, new Observer<List<Employee>>() {
      @Override
      public void onChanged(@Nullable List<Employee> employees) {
        employeeDataAdapter.setEmployees((ArrayList<Employee>) employees);
        Log.e("getAllEmployee", "getAllEmployee: " + employees.size());
      }
    });
  }
}
